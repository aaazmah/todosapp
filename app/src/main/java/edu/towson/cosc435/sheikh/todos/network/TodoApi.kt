package edu.towson.cosc435.sheikh.todos.network

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import edu.towson.cosc435.sheikh.todos.interfaces.ITodoController
import edu.towson.cosc435.sheikh.todos.model.Todo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

interface ITodoApi {
    suspend fun fetchSongs(): List<Todo>
    suspend fun fetchIcon(imageUrl: String): Bitmap?
}

class TodoApi(private val controller: ITodoController) : ITodoApi {

    private val URL = "https://my-json-server.typicode.com/rvalis-towson/todos_api/todos"

    override suspend fun fetchSongs(): List<Todo> {
        return withContext(Dispatchers.IO) {
            val client: OkHttpClient = OkHttpClient()
            val request: Request = Request.Builder()
                .url(URL)
                .get()
                .build()
            val response: Response = client.newCall(request).execute()
            val json = response.body?.string()
            if(json != null) {
                val gson = Gson()
                val listType = object: TypeToken<List<Todo>>() {}.type
                val songs: List<Todo> = gson.fromJson<List<Todo>>(json, listType)
                songs
            } else {
                listOf()
            }
        }
    }

    override suspend fun fetchIcon(imageUrl: String): Bitmap? {
        return withContext(Dispatchers.IO) {
            val filename = getImageFilename(imageUrl)
            val bitmap = controller.checkCache(filename)
            if (bitmap != null) {
                bitmap
            } else {
                val client = OkHttpClient()
                val request = Request.Builder()
                    .url(imageUrl)
                    .get()
                    .build()
                val response = client.newCall(request).execute()
                val byteStream = response.body?.byteStream()
                val bitmap = BitmapFactory.decodeStream(byteStream)
                if(bitmap != null) {
                    controller.cacheIcon(filename, bitmap)
                }
                bitmap
            }
        }
    }

    private fun getImageFilename(url: String): String {
        val urlObj = java.net.URL(url)
        val query = urlObj.query
        val filename = query.replace("=", "")
        return filename.plus(".jpg")
    }
}