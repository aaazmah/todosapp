package edu.towson.cosc435.sheikh.todos.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class Todo(
        @PrimaryKey
        val id: UUID,
        val title: String,
        val contents: String,
        var isCompleted: Boolean,
        @SerializedName("image_url")
        val image: String?,
        var date: String
)
