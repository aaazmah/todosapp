package edu.towson.cosc435.sheikh.todos

import android.system.Os.remove
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import edu.towson.cosc435.sheikh.todos.interfaces.ITodoController
import edu.towson.cosc435.sheikh.todos.model.Todo
import edu.towson.cosc435.sheikh.todos.network.TodoApi

class todoAdapter(val controller: ITodoController) : RecyclerView.Adapter<TodoViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {

        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.todo_view, parent, false)
        val holder = TodoViewHolder(view)

        holder.todoIsCompletecb?.setOnClickListener {
            val position = holder.adapterPosition
            controller.markCompleted(position)
        }

        view.setOnClickListener {
            controller.editTodo(holder.adapterPosition)
        }

        view.setOnLongClickListener {
            controller.deleteTodo(holder.adapterPosition)
            true
        }

//        holder.onLongClick(view)
//
//        holder.deleteBTN?.setOnClickListener {
//            val position = holder.adapterPosition
//            controller.deleteTodo(position)
//            notifyItemRemoved(position)
//        }
//        holder.todoIsCompletecb?.setOnClickListener {
//            val position = holder.adapterPosition
//            controller.markCompleted(position)
//            notifyItemChanged(position)
//        }
        return  holder
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        val todo = controller.getTodo(position)
        holder.bindTodo(todo, controller)
    }

    override fun getItemCount(): Int {
        return controller.getTodos().size
    }


}


class TodoViewHolder(view: View ) : RecyclerView.ViewHolder(view){
    private val todoTitleTextView = itemView.findViewById<TextView>(R.id.titleContent)
    private val todoContentTextView = itemView.findViewById<TextView>(R.id.todoContent)
    private val todoDateTextView = itemView.findViewById<TextView>(R.id.dueDate)
    val todoIsCompletecb: CheckBox ? = itemView.findViewById(R.id.checkBox2)
    //val deleteBTN :Button? = itemView.findViewById(R.id.deleteButton)
    private val imageView = itemView.findViewById<ImageView>(R.id.imageView)
    private val iconProgress = itemView.findViewById<ProgressBar>(R.id.iconProgress)

    fun bindTodo(todo: Todo, controller: ITodoController){
        todoTitleTextView.text = todo.title
        todoContentTextView.text = todo.contents
        todoIsCompletecb?.isChecked = todo.isCompleted
        todoDateTextView.text = todo.date
        val todoApi = TodoApi(controller)
        imageView.visibility = View.INVISIBLE
        iconProgress.visibility = View.VISIBLE
        if(todo.image != null) {
            controller.runAsync {
                val bitmap = todoApi.fetchIcon(todo.image)
                imageView.setImageBitmap(bitmap)
                iconProgress.visibility = View.INVISIBLE
                imageView.visibility = View.VISIBLE
            }
        } else {
            iconProgress.visibility = View.INVISIBLE
            imageView.visibility = View.VISIBLE
            imageView.setImageBitmap(null)
        }

    }




}

