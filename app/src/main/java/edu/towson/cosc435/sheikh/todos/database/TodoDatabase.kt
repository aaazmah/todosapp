package edu.towson.cosc435.sheikh.todos.database

import androidx.room.*
import edu.towson.cosc435.sheikh.todos.model.Todo
import java.util.*

@Dao
interface ITodoDao{
    @Query("Select id, title, contents, isCompleted, image, date from Todo")
    fun getTodos(): List<Todo>

    @Query("Select * from Todo where title = :title")
    fun getTodo(title: String) : Todo

    @Update
    fun updateTodo(todo: Todo)

    @Delete
    fun deleteTodo(todo: Todo)

    @Insert
    fun addTodo(todo: Todo)

}

class UUIDTypeConverter{
    @TypeConverter
    fun toString(uuid: UUID): String{
        return uuid.toString()
    }

    @TypeConverter
    fun fromString(uuid: String) : UUID {
        return  UUID.fromString(uuid)
    }
}

@Database(entities = [Todo::class], version = 1, exportSchema = false)
@TypeConverters(UUIDTypeConverter::class)
abstract class TodoDatabase : RoomDatabase(){
    abstract fun todoDao(): ITodoDao

}