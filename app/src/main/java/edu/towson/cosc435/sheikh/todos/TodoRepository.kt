package edu.towson.cosc435.sheikh.todos
//
//import edu.towson.cosc435.sheikh.todos.interfaces.ITodoRepository
//import edu.towson.cosc435.sheikh.todos.model.Todo
//import java.util.*
//
//class TodoRepository: ITodoRepository {
//
//
//private val todos: MutableList<Todo> = mutableListOf()
//
//    init {
//        (0..10).forEach { i ->
//            todos.add(Todo(
//                     id = UUID.randomUUID(),
//                     title =  "Title $i",
//                     contents =  "Content $i",
//                     isCompleted = false,
//                     image =  "image $i",
//                     dateCreated = Date()
//            ))
//        }
//    }
//    override fun getCount(): Int {
//        return todos.size
//    }
//
//    override fun getTodo(idx: Int): Todo {
//        if (idx < 0) return todos[0]
//        if (idx >= getCount()) return todos[todos.size - 1]
//        return todos[idx]
//    }
//
//    override fun getTodos(): List<Todo> {
//        return todos
//    }
//
//    override fun deleteTodo(idx: Int) {
//        if(idx < 0 || idx >= getCount()) throw Exception("Index out of bounds")
//        todos.removeAt(idx)
//    }
//
//    override fun addTodo(todo: Todo) {
//        todos.add(todo)
//    }
//
//
//}