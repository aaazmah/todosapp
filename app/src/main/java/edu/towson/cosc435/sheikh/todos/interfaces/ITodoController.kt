package edu.towson.cosc435.sheikh.todos.interfaces

import android.graphics.Bitmap
import edu.towson.cosc435.sheikh.todos.model.Todo

interface ITodoController {
    fun deleteTodo(idx: Int)
    fun editTodo(idx: Int)
    fun markCompleted(idx: Int)


    // Recycler View functions
    fun getTodo(idx: Int): Todo
    fun getTodos(): List<Todo>

    fun runAsync(block: suspend () -> Unit)
    fun checkCache(filename: String): Bitmap?
    fun cacheIcon(filename: String, icon: Bitmap)


}