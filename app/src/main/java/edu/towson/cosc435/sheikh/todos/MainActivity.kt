package edu.towson.cosc435.sheikh.todos

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import edu.towson.cosc435.sheikh.todos.database.TodoDatabaseRepository
import edu.towson.cosc435.sheikh.todos.interfaces.ITodoController
import edu.towson.cosc435.sheikh.todos.model.Todo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File


class MainActivity : AppCompatActivity(), ITodoController {

    // Initialize a variable that contains a button property
    private lateinit var addTodoButton: Button
    private lateinit var  recyclerView: RecyclerView
    private val scope = CoroutineScope(Dispatchers.Main)
    private lateinit var todosRepository: TodoDatabaseRepository
    private var editingIdx = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        grabWidgets()
        addTodoButton.setOnClickListener {launchActivity()}

        recyclerView.adapter = todoAdapter(this)

        recyclerView.layoutManager = LinearLayoutManager(this)

    }

    // Separate function to hold all the widgets
    private fun grabWidgets(){
        // The launchButton variable now holds the launchactivity button from the layout

        addTodoButton = findViewById(R.id.addTodoBtn)
        recyclerView = findViewById(R.id.recyclerView)
        todosRepository = TodoDatabaseRepository(application)


    }

    private fun launchActivity(){
        val intent = Intent(this, NewTodoActivity::class.java)
        startActivityForResult(intent, REQUEST_CODE)
    }

    // Override to get the data back
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            REQUEST_CODE -> {
                when (resultCode) {
                    RESULT_OK -> {
                        val json = data?.getStringExtra(NewTodoActivity.INFO_KEY)
                        if (json != null) {
                            val todo = Gson().fromJson<Todo>(json, Todo::class.java)
                            todosRepository.addTodo(todo)
                            recyclerView.adapter?.notifyDataSetChanged()
                            recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context, DividerItemDecoration.VERTICAL))
                        }
                    }
                }
            }
        }
    }



    override fun deleteTodo(idx: Int) {
        confirmDelete {
            todosRepository.deleteTodo(idx)
            recyclerView.adapter?.notifyItemRemoved(idx)
        }
    }

    override fun editTodo(pos: Int) {
        editingIdx = pos
        val intent = Intent(this, NewTodoActivity::class.java)
        val todo = todosRepository.getTodo(pos)
        intent.putExtra(NewTodoActivity.TODO, Gson().toJson(todo))
        startActivityForResult(intent,
                REQUEST_CODE
        )
    }

    override fun markCompleted(idx: Int) {
        val todo = todosRepository.getTodo( idx)
        todo.isCompleted = !todo.isCompleted
        todosRepository.updateTodo(idx, todo)
        recyclerView.adapter?.notifyItemChanged(idx)
    }

    override fun getTodo(idx: Int): Todo {
        return todosRepository.getTodo(idx)
    }

    override fun getTodos(): List<Todo> {
        return todosRepository.getTodos()
    }

    override fun runAsync(block: suspend () -> Unit) {
        scope.launch { block() }
    }

    override fun checkCache(filename: String): Bitmap? {
        val file = File(cacheDir, filename)
        return if(file.exists()) {
            val input = file.inputStream()
            BitmapFactory.decodeStream(input)
        } else {
            null
        }
    }

    override fun cacheIcon(filename: String, icon: Bitmap) {
        val file = File(cacheDir, filename)
        val output = file.outputStream()
        icon.compress(Bitmap.CompressFormat.JPEG, 100, output)
    }

    private fun confirmDelete(callback: () -> Unit) {
        AlertDialog
                .Builder(this)
                .setMessage("Delete Todo?")
                .setPositiveButton("Delete") { _, _ ->
                    callback()
                }
                .setNegativeButton("Cancel") { _, _ -> }
                .create()
                .show()
    }


    companion object {
        const val REQUEST_CODE = 1;
        const val TAG = "MainActivity"
    }



}