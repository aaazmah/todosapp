package edu.towson.cosc435.sheikh.todos.database

import android.app.Application
import androidx.room.Room
import edu.towson.cosc435.sheikh.todos.interfaces.ITodoRepository
import edu.towson.cosc435.sheikh.todos.model.Todo

class TodoDatabaseRepository(app: Application) : ITodoRepository {

    private val todoList: MutableList<Todo> = mutableListOf()
    private val todoDb: TodoDatabase

    init {
        todoDb = Room.databaseBuilder(
            app,
            TodoDatabase::class.java,
            "todos.db"
        ).allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
        todoList.addAll(todoDb.todoDao().getTodos())
    }


    override fun getCount(): Int {
        return todoList.size
    }

    override fun getTodo(idx: Int): Todo {
        return todoList[idx]
    }

    override fun getTodos(): List<Todo> {
        return todoList
    }

    override fun deleteTodo(idx: Int) {
        val todo = todoList[idx]
        todoDb.todoDao().deleteTodo(todo)
        todoList.clear()
        todoList.addAll(todoDb.todoDao().getTodos())
    }

    override fun updateTodo(idx: Int, todo: Todo) {
        todoDb.todoDao().updateTodo(todo)
        todoList.clear()
        todoList.addAll(todoDb.todoDao().getTodos())
    }

    override fun addTodo(todo: Todo) {
        todoDb.todoDao().addTodo(todo)
        todoList.clear()
        todoList.addAll(todoDb.todoDao().getTodos())
    }
}